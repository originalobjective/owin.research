﻿using System;
using System.IdentityModel.Tokens;
using System.Net;
using System.Security.Claims;
using System.Text;
using FluentAssertions;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Microsoft.Owin.Testing;
using Owin;
using Xunit;

namespace OWIN.Research.Tests
{
    /// <summary>
    /// Some learnings about AuthZ.
    /// </summary>
    public class BearerTokenAuthZTests
    {
        /// <summary>
        /// Creates a <see cref="TestServer"/> with Jwt AuthN middle-ware
        /// and custom AuthZ middle-ware.
        /// </summary>
        /// <returns></returns>
        private TestServer CreateHelloReturnServerWithJwtBearerAuthN()
        {
            var s = TestServer.Create(builder =>
            {
                builder.UseJwtBearerAuthentication(new JwtBearerAuthenticationOptions
                {
                    AuthenticationMode = AuthenticationMode.Active,
                    AllowedAudiences = new[] {JwtAudience},
                    IssuerSecurityTokenProviders =
                        new[]
                        {
                            new SymmetricKeyIssuerSecurityTokenProvider(JwtIssuer,
                                Convert.ToBase64String(Encoding.ASCII.GetBytes(JwtSymmetricKey)))
                        }
                });

                // AuthZ middleware
                builder.Use(async (ctx, func) =>
                {
                    if (ctx.Authentication.User == null)
                    {
                        ctx.Response.StatusCode = 401;
                        return;
                    } 
                    if(!ctx.Authentication.User.HasClaim("scope","colsapp"))
                    {
                        ctx.Response.StatusCode = 403;
                        return;
                    }
                    await func();
                });

                builder.Run(context => context.Response.WriteAsync("Hello"));
            });
            return s;
        }

        [Fact]
        public async void ShouldDenyWhenNoBearer()
        {
            using (var s = CreateHelloReturnServerWithJwtBearerAuthN())
            {
                var request = s.CreateRequest("/");
                var response = await request.GetAsync();
                response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Unauthorized, "a bearer token is missing from the request should result in a 401 (Unauthorized)");
            }
        }

        [Fact]
        public async void ShouldDenyWhenMissingScopeOfColsApp()
        {
            using (var s = CreateHelloReturnServerWithJwtBearerAuthN())
            {
                var request = s.CreateRequest("/").AddHeader("Authorization", "Bearer " + NewBearerToken());
                var response = await request.GetAsync();
                response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Forbidden, "a bearer token which does not have an expected scope should result in a 403 (Forbidden)");
            }
        }

        [Fact]
        public async void ShouldAllowWithScopeOfColsApp()
        {
            using (var s = CreateHelloReturnServerWithJwtBearerAuthN())
            {
                // add scope=colsapp
                var request = s.CreateRequest("/").AddHeader("Authorization","Bearer " + NewBearerToken(new Claim("scope","colsapp")));
                var response = await request.GetAsync();
                response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK,"a bearer token with exected claim should be allowed");
            }
        }

        private const string JwtAudience = "http://chwilliamson.me.uk/audience";
        private const string JwtIssuer = "http://chwilliamson.me.uk";
        private const string JwtSymmetricKey = "My fancy key for owin";

        /// <summary>
        /// Creates a bearer token string.
        /// </summary>
        /// <param name="claims"></param>
        /// <returns></returns>
        private string NewBearerToken(params Claim[] claims)
        {
            var securityToken = new JwtSecurityToken(
                JwtIssuer,
                JwtAudience,
                claims,
                DateTime.UtcNow,
                DateTime.UtcNow.AddDays(1),
                new SigningCredentials(new InMemorySymmetricSecurityKey(Encoding.ASCII.GetBytes(JwtSymmetricKey)),
                    "http://www.w3.org/2001/04/xmldsig-more#hmac-sha256",
                    "http://www.w3.org/2001/04/xmlenc#sha256"));

            return new JwtSecurityTokenHandler().WriteToken(securityToken);
        }
    }
}
