﻿using System;
using System.Net;
using Microsoft.Owin.Testing;
using Xunit;

namespace OWIN.Research.Tests
{
    /// <summary>
    /// Performs tests against <see cref="HelloWorldStartup"/>.
    /// </summary>
    public class HelloWorldStartupTests
    {
        private readonly Uri helloWorldUri = new Uri("http://localhost:9881/helloworld/");
        
        /// <summary>
        /// A simple test to create a self host.
        /// </summary>
        [Fact]
        public void ShouldResponseWithHelloWorld()
        {
            using (var s =TestServer.Create<HelloWorldStartup>())
            {
                var request = s.CreateRequest(helloWorldUri.ToString());
                //var sr = new StreamWriter(request.GetRequestStream());
                //sr.WriteLine("Hello There");
                var response =  request.GetAsync().Result;
                Assert.Equal(HttpStatusCode.OK,response.StatusCode);
                Assert.Equal("Hello World", response.Content.ReadAsStringAsync().Result);
            }
        }
    }
}
