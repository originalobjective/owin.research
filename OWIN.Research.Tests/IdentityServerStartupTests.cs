﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using FluentAssertions;
using HtmlAgilityPack;
using log4net;
using log4net.Core;
using log4net.Repository.Hierarchy;
using Microsoft.Owin.Testing;
using Newtonsoft.Json.Linq;
using Thinktecture.IdentityServer.Core.Logging;
using Thinktecture.IdentityServer.Core.Logging.LogProviders;
using Xunit;
using Xunit.Abstractions;
using ILog = log4net.ILog;

namespace OWIN.Research.Tests
{
    /// <summary>
    /// These tests are writen to aid my understanding of IdentityServer
    /// OpenId Connect implementation.
    /// </summary>
    public class IdentityServerStartupTests
    {
        private readonly IAppenderAttachable attachable;
        private readonly TestOutputAppender appender;
        private readonly ILog logger = LogManager.GetLogger(typeof (IdentityServerStartupTests));

        public IdentityServerStartupTests(ITestOutputHelper output)
        {
            var root = ((Hierarchy)LogManager.GetRepository()).Root;
            attachable = root;
            appender = new TestOutputAppender(output);
            if (attachable != null)
                attachable.AddAppender(appender);

            LogProvider.SetCurrentLogProvider(new Log4NetLogProvider());
        }

        private TestServer CreateTestIdentityServer()
        {
            var server = TestServer.Create(app =>
            {
                var assembly = typeof (IdentityServerStartupTests).Assembly;

                // use the IdentityServer test certificate
                using (var stream = assembly.GetManifestResourceStream("OWIN.Research.Tests.idsrv3test.pfx"))
                {
                    var ms = new MemoryStream();
                    stream.CopyTo(ms);
                    var cert = new X509Certificate2(ms.ToArray(), "idsrv3test");

                    new IdentityServerStartup(cert).Configuration(app);
                }
            });
            return server;
        }

        /// <summary>
        /// A utility method to handle the EndUser AuthN.
        /// </summary>
        /// <param name="server">The test server to use.</param>
        /// <param name="authUri">The authorization Uri.</param>
        /// <returns></returns>
        private async Task<HttpResponseMessage> ProcessEndUserAuthN(TestServer server, string authUri)
        {
            var request =
                    server.CreateRequest(authUri);
            var response = await request.GetAsync();

            // IdP will ask user to signin
            response.Headers.Location.AbsoluteUri.Should().StartWith("http://idp/login?signin=");
            response.Headers.Should().Contain(a => a.Key == "Set-Cookie", "expecting Set-Cookie header");

            var signInMessageCookie = ParseCookie(response.Headers.First(c => c.Key == "Set-Cookie").Value.First());

            // Handled by the browser
            // visit /login?signin=????
            var signInRequest = server.CreateRequest("/login" + response.Headers.Location.Query);
            signInRequest.And(message => message.Headers.Add("Cookie", string.Format("{0}={1}", signInMessageCookie.Name, signInMessageCookie.Value)));

            logger.Debug("Getting /sigin");
            var signInResponse = await signInRequest.GetAsync();
            signInResponse.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
            signInResponse.Content.Headers.ContentType.MediaType.ShouldBeEquivalentTo("text/html");
            var signInResponseContent = await signInResponse.Content.ReadAsStringAsync();

            var doc = new HtmlDocument();
            doc.LoadHtml(signInResponseContent);

            var idxCookie = ParseCookie(signInResponse.Headers.First(c => c.Key == "Set-Cookie" && c.Value.First().StartsWith("idsrv.xsrf")).Value.First());

            var rawScript = doc.DocumentNode.SelectSingleNode("//script[@id='modelJson']").InnerText;
            var scriptJson = HttpUtility.HtmlDecode(rawScript);

            var json = JObject.Parse(scriptJson);
            var antiForgery = json["antiForgery"];
            var antiForgertName = antiForgery["name"].Value<string>();
            var antiForgertValue = antiForgery["value"].Value<string>();

            logger.DebugFormat("AntiForgery Token {0}={1}", antiForgertName, antiForgertValue);

            // need to post a login
            var loginRequest = server.CreateRequest("/login" + response.Headers.Location.Query);
            loginRequest.And(message =>
            {
                message.Content = new FormUrlEncodedContent(new[]
                {
                        new KeyValuePair<string, string>(antiForgertName, antiForgertValue),
                        new KeyValuePair<string, string>("username", "colin"),
                        new KeyValuePair<string, string>("password", "password"),
                    });
                message.Headers.Add("Cookie",
                    string.Format("{0}={1}", signInMessageCookie.Name, signInMessageCookie.Value));
                message.Headers.Add("Cookie",
                    string.Format("{0}={1}", idxCookie.Name, idxCookie.Value));

            });
            logger.Debug("Posting /signin");
            var loginResponse = await loginRequest.PostAsync();

            // back to authorize
            loginResponse.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Redirect);
            loginResponse.Headers.Location.AbsoluteUri.Should().StartWith("http://idp/connect/authorize");

            var idsvrCookie =
                ParseCookie(
                    loginResponse.Headers
                        .Where(c => c.Key == "Set-Cookie")
                        .SelectMany(c => c.Value)
                        .Single(c => c.StartsWith("idsrv=")));

            logger.DebugFormat("Redirecting to connect/authorize");
            // get the callback
            var callbackRequest = server.CreateRequest("/connect/authorize" + loginResponse.Headers.Location.Query);
            callbackRequest.And(message =>
            {
                message.Headers.Add("Cookie", string.Format("{0}={1}", idxCookie.Name, idxCookie.Value));
                message.Headers.Add("Cookie", string.Format("{0}={1}", idsvrCookie.Name, idsvrCookie.Value));
            });

            return await callbackRequest.GetAsync();
        }

        /// <summary>
        /// WIP: Still need to determine a use case for this flow.
        /// I am wondering whether this is for Thick Clients?
        /// </summary>
        [Fact]
        public async void UnderstandingHybridFlow()
        {
            using (var server = CreateTestIdentityServer())
            {
                // can use get or post (using get)
                var state = Guid.NewGuid().ToString("N");
                var callbackResponse = await ProcessEndUserAuthN(
                        server,
                        "connect/authorize?client_id=chw_hybrid&response_type=code id_token token&scope=openid profile special_access&state=" + state +
                        "&redirect_uri=" + HttpUtility.UrlEncode("http://localhost/Foo") + "&nonce=" + Guid.NewGuid().ToString("N"));

                // redirect back to client
                callbackResponse.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Redirect, "should redirect to client");
                callbackResponse.Headers.Location.AbsoluteUri.Should().StartWith("http://localhost/Foo#code=");

                // Okay, everything after # is technically not seen by the web server using a web browser.
                // In Auth Code Flow; this would be ?code= where the web server will use basic auth to fetch id_token and token to the token endpoint
                // Maybe this is for Think clients?
            }
        }


        /// <summary>
        /// See http://openid.net/specs/openid-connect-core-1_0.html#CodeFlowSteps
        /// 
        /// The Authorization Code Flow goes through the following steps. 
        /// 1.Client prepares an Authentication Request containing the desired request parameters.
        /// 2.Client sends the request to the Authorization Server. 
        /// 3.Authorization Server Authenticates the End-User.
        /// 4.Authorization Server obtains End-User Consent/Authorization.
        /// 5.Authorization Server sends the End-User back to the Client with an Authorization Code.
        /// 6.Client requests a response using the Authorization Code at the Token Endpoint.
        /// 7.Client receives a response that contains an ID Token and Access Token in the response body.
        /// 8.Client validates the ID token and retrieves the End-User's Subject Identifier.
        /// </summary>
        [Fact]
        public async void UnderstandingAuthorizationCodeFlow()
        {
            using (var server = CreateTestIdentityServer())
            {
                // can use get or post (using get)
                var state = Guid.NewGuid().ToString("N");
                
                // get the callback
                var callbackResponse = await ProcessEndUserAuthN(server,
                        "connect/authorize?client_id=chw_code&response_type=code&scope=openid profile special_access&state=" + state +
                        "&redirect_uri=" + HttpUtility.UrlEncode("http://localhost/Foo") + "&nonce=" + Guid.NewGuid().ToString("N"));
                
                // redirect back to client
                callbackResponse.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Redirect, "should redirect to client");
                callbackResponse.Headers.Location.AbsoluteUri.Should().StartWith("http://localhost/Foo?code=");

                // not we get the code
                var code = callbackResponse.Headers.Location.AbsoluteUri.Substring("http://localhost/Foo?code=".Length).Split('&')[0];

                // the server requests a token using the code
                var tokenRequest = server.CreateRequest("connect/token")
                    .And(message =>
                    {
                        message.Content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                        {
                            new KeyValuePair<string, string>("grant_type", "authorization_code"),
                            new KeyValuePair<string, string>("code", code),
                            new KeyValuePair<string, string>("redirect_uri", "http://localhost/Foo")
                        });

                    })
                    .AddHeader("Authorization",
                        "Basic " + Convert.ToBase64String(Encoding.ASCII.GetBytes("chw_code:secret")));

                var tokenResponse = await tokenRequest.PostAsync();

                tokenResponse.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
                tokenResponse.Content.Headers.ContentType.MediaType.ShouldBeEquivalentTo("application/json");
                var content = await tokenResponse.Content.ReadAsStringAsync();

                content.Should().NotBeNullOrWhiteSpace();

                var tokenJson = JObject.Parse(content);

                var accessToken = tokenJson["access_token"].Value<string>();

                var handler = new JwtSecurityTokenHandler();

                // validation should be applied when in an Api (not including for now)
                var securityToken = handler.ReadToken(accessToken) as JwtSecurityToken;

                securityToken.Should().NotBeNull();
                securityToken.Claims.Should().Contain(a => a.Type == "scope" && a.Value == "special_access", "special_access scope claim expected");
            }
        }

        /// <summary>
        /// A tests to allow me to understand
        /// 
        /// See http://tools.ietf.org/html/rfc6749
        /// 
        /// 4.4.  Client Credentials Grant
        /// 
        /// The client can request an access token using only its client
        /// credentials(or other supported means of authentication) when the
        /// client is requesting access to the protected resources under its
        /// control, or those of another resource owner that have been previously
        /// arranged with the authorization server(the method of which is beyond
        /// the scope of this specification).
        /// </summary>
        [Fact]
        public async void UnderstandingClientCredentialsGrant()
        {
            using (var server = CreateTestIdentityServer())
            {
                var request = server.CreateRequest("connect/token");
                request.And(message =>
                {
                    message.Content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                    {
                        new KeyValuePair<string, string>("client_id", "chw"),
                        new KeyValuePair<string, string>("client_secret", "secret"),
                        new KeyValuePair<string, string>("grant_type", "client_credentials"),
                        new KeyValuePair<string, string>("scope", "profile special_access"),
                    });
                });
              
                var response = await request.PostAsync();
                response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
                var content = await response.Content.ReadAsStringAsync();
                content.Should().NotBeNullOrWhiteSpace();
                var payload = JwtPayload.Deserialize(content);

                // access_token contains the claims
                var token = payload["access_token"] as string;
                
                var handler = new JwtSecurityTokenHandler();

                // validation should be applied when in an Api (not including for now)
                var securityToken = handler.ReadToken(token) as JwtSecurityToken;

                securityToken.Should().NotBeNull();
                securityToken.Claims.Should().Contain(a => a.Type == "scope" && a.Value=="special_access","special_access scope claim expected");

                /*
                * I am assuming claims transformation can interpret this?
                * Example: 
                *  An API may allow deletion for chw:delete claim.
                *
                * This means we could transform client_chw:delete to chw_delete...
                */
                securityToken.Claims.Should().Contain(a => a.Type == "client_chw:delete","special_access scope has chw:delete");
            }
        }

        /// <summary>
        /// Request a token for a user.
        /// </summary>
        [Fact]
        public async void UnderstandingResourceOwner()
        {
            using (var server = CreateTestIdentityServer())
            {
                var request = server.CreateRequest("connect/token");
                request.And(message =>
                {
                    message.Content = new FormUrlEncodedContent(new List<KeyValuePair<string, string>>()
                    {
                        new KeyValuePair<string, string>("client_id", "chw_resource"),
                        new KeyValuePair<string, string>("client_secret", "secret"),
                        new KeyValuePair<string, string>("grant_type", "password"),
                        new KeyValuePair<string, string>("scope", "profile special_access"),
                        new KeyValuePair<string, string>("username", "colin"),
                        new KeyValuePair<string, string>("password", "password")
                    });
                });

                var response = await request.PostAsync();
                response.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK);
                var content = await response.Content.ReadAsStringAsync();
                content.Should().NotBeNullOrWhiteSpace();
                var payload = JwtPayload.Deserialize(content);

                // access_token contains the claims
                var token = payload["access_token"] as string;

                var handler = new JwtSecurityTokenHandler();

                // validation should be applied when in an Api (not including for now)
                var securityToken = handler.ReadToken(token) as JwtSecurityToken;

                securityToken.Should().NotBeNull();
                securityToken.Claims.Should().Contain(a => a.Type == "scope" && a.Value == "special_access", "special_access scope claim expected");
                securityToken.Claims.Should().Contain(a => a.Type=="sub" && a.Value == "12345");
            }
        }

        private Cookie ParseCookie(string cookie)
        {
            var setcookieSplit = cookie.Split(';');
            var setcookieSplit2 = setcookieSplit[0].Split('=');
            var name = setcookieSplit2[0];
            var value = setcookieSplit2[1];

            return new Cookie(name,value);
        }

        /// <summary>
        /// Seet http://openid.net/specs/openid-connect-implicit-1_0.html#ImplicitFlow
        /// </summary>
        [Fact]
        public async void UnderstandingImplicitFlow()
        {
            using (var server = CreateTestIdentityServer())
            {
                var state = Guid.NewGuid().ToString("N");

                var callbackResponse = await
                    ProcessEndUserAuthN(server,
                        "connect/authorize?client_id=chw_implicit&response_type=id_token%20token&scope=openid profile special_access&state=" +
                        state +
                        "&redirect_uri=" + HttpUtility.UrlEncode("http://localhost/Foo") + "&nonce=" +
                        Guid.NewGuid().ToString("N"));

                callbackResponse.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.Redirect,"should redirect to client");
                callbackResponse.Headers.Location.AbsoluteUri.Should().StartWith("http://localhost/Foo");

                // Example:
                // http://localhost/Foo#id_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImEzck1VZ01Gdjl0UGNsTGE2eUYzekFrZnF1RSIsImtpZCI6ImEzck1VZ01Gdjl0UGNsTGE2eUYzekFrZnF1RSJ9.eyJub25jZSI6ImMxNGQxZjVhYTkwODQ1NThiYTE1ZGUxMDZiNmEwZmEwIiwiaWF0IjoxNDQxODA4OTIxLCJhdF9oYXNoIjoiRHFGRHVyaThyaFRKQ0RkLUxhZHpTZyIsInN1YiI6IjEyMzQ1IiwiYW1yIjoicGFzc3dvcmQiLCJhdXRoX3RpbWUiOjE0NDE4MDg5MjEsImlkcCI6Imlkc3J2IiwiaXNzIjoiaHR0cHM6Ly9pZHNydjMuY29tIiwiYXVkIjoiY2h3X2ltcGxpY2l0IiwiZXhwIjoxNDQxODA5MjIxLCJuYmYiOjE0NDE4MDg5MjF9.EHAN1oU1IrUOhzLhuZ_dOS25HrwR36O9_fR2U1dnJh-qw4SSAOoezSWtVu7w4BsXwvBAd2SUI6o3gD0WXBVo0qy6eJEyvIX74wXrP0XGhqfJ6dP8iUQmlSZ4ZYvrA4V7EWvdUInNgguRlUvgG6cYHhRK5XCxzd1iZ6a7xHs7cGFfR6B59-7o373UNbN-isOpwiZS17ucu6NBYKLsN6VpDifHVYdQV3Q5q2qgecLPvPyqJyjssvqHBTsYadc1uQcDfTK7r0ruBJwW4IsRm5UwcOJeAbiSO5wM0T2Mk2QKpOsZr1O_lSlbpB7H_ZsabgbcJW9dhBhp6dxum4mJHaMKIQ&access_token=eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6ImEzck1VZ01Gdjl0UGNsTGE2eUYzekFrZnF1RSIsImtpZCI6ImEzck1VZ01Gdjl0UGNsTGE2eUYzekFrZnF1RSJ9.eyJjbGllbnRfaWQiOiJjaHdfaW1wbGljaXQiLCJzY29wZSI6WyJvcGVuaWQiLCJwcm9maWxlIl0sInN1YiI6IjEyMzQ1IiwiYW1yIjoicGFzc3dvcmQiLCJhdXRoX3RpbWUiOjE0NDE4MDg5MjEsImlkcCI6Imlkc3J2IiwiaXNzIjoiaHR0cHM6Ly9pZHNydjMuY29tIiwiYXVkIjoiaHR0cHM6Ly9pZHNydjMuY29tL3Jlc291cmNlcyIsImV4cCI6MTQ0MTgwOTI4MSwibmJmIjoxNDQxODA4OTIxfQ.Vm6HKzR2iyd8nMJopzNjg7fxJMsAFjMypToN1oTv6lDLt_yI4PWWVzLmNLjCZCf0qVGMOCaChhnuXBIS2-gFga8VdfHIfO7-R92js2w-lPGwvzqKtQT6x4r3CaG1IzByX9vwfOKQX-1LlATjZsVfLp9oRTuYklLrW9nJxpzT9UrNZMskYaghsKTDeLBlhTpd81EaZ38rSW06ZKcyOr291AauJmj4lmxk_52H0ZsXYX8Gt2sZHHMZODZYEqQG_M-iuYua4kA7MNeR_CHyGS7I1-6vXPqF3SGDNK-iv_8Pzwy-EYUnPZbqqrUpqHK2rLS-LZITWxmq7AEmTyDo3f2iSQ&token_type=Bearer&expires_in=360&scope=openid%20profile&state=53f838e549c54c7d910afb6f2d33def0
                var uriSplit = callbackResponse.Headers.Location.AbsoluteUri.Split('#');
                var nameAndValues = uriSplit[1].Split('&');
                
                var accessToken = nameAndValues.First(a => a.StartsWith("access_token")).Split('=')[1];
                var idToken = nameAndValues.First(a => a.StartsWith("id_token")).Split('=')[1];

                // the user identity
                var idSecurityToken = new JwtSecurityTokenHandler().ReadToken(idToken) as JwtSecurityToken;
                idSecurityToken.Should().NotBeNull();
                
                // token to access resources
                var securityToken = new JwtSecurityTokenHandler().ReadToken(accessToken) as JwtSecurityToken;

                securityToken.Should().NotBeNull();
                securityToken.Claims.Should().Contain(a => a.Type == "scope" && a.Value == "special_access", "special_access scope claim expected");
                securityToken.Claims.Should().Contain(a => a.Type == "special101" && a.Value == "admin", "expected user claim special101 to be returned");
            }
        }

        public void Dispose()
        {
            attachable.RemoveAppender(appender);
        }

    }
}