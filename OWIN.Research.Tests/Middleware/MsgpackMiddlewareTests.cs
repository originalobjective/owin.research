﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using Microsoft.Owin.Testing;
using MsgPack.Serialization;
using OWIN.Research.Middleware;
using OWIN.Research.Models;
using Xunit;

namespace OWIN.Research.Tests.Middleware
{
    /// <summary>
    /// Some very basic tests using msg pack.
    /// </summary>
    public class MsgpackMiddlewareTests
    {
        private readonly Uri helloWorldUri = new Uri("http://localhost:9881/msgpack/");
        
        /// <summary>
        /// Test msg-pack middleware by sending a request with the name 'Colin'
        /// expecting a return of <see cref="Hello"/> with the <see cref="Hello.Name"/>
        /// equal to 'Colin'.
        /// </summary>
        [Fact]
        public void ShouldSendRecvBasicMessage()
        {
            using (var s = TestServer.Create(app =>
            {
                // user a single 
                app.Use(typeof (MsgpackMiddleware));
            }))
            {
                var msReqStream = new MemoryStream();
                var helloReq = new HelloRequest { Name = "Colin" };
                
                MessagePackSerializer.Get<HelloRequest>().Pack(msReqStream,helloReq);
                msReqStream.Seek(0, 0);

                var response = s.HttpClient.PostAsync(helloWorldUri, new StreamContent(msReqStream)).Result;
                
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                var stream = response.Content.ReadAsStreamAsync().Result;
                var hello = MessagePackSerializer.Get<Hello>().Unpack(stream);
                Assert.Equal(HttpStatusCode.OK, response.StatusCode);
                Assert.Equal("Colin",hello.Name);
            }
        }
    }
}
