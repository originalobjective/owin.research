﻿using System.Net;
using FluentAssertions;
using HtmlAgilityPack;
using Microsoft.Owin.Testing;
using Xunit;

namespace OWIN.Research.Tests
{
    public class NancyStartupTests
    {
        /// <summary>
        /// Calling a simple nancy module that returns 'Hello'.
        /// </summary>
        [Fact]
        public async void HelloOwinTest()
        {
            using (var server = TestServer.Create<NancyStartup>())
            {
                var req = server.CreateRequest("/hello");
                var res = await req.GetAsync();
                var html = await res.Content.ReadAsStringAsync();
                html.ShouldBeEquivalentTo("Hello");
            }
        }

        [Fact]
        public async void RazorOwinTest()
        {
            using (var server = TestServer.Create<NancyStartup>())
            {
                var req = server.CreateRequest("/razor");
                var res = await req.GetAsync();

                res.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK,"expecting an ok response");
                res.Content.Headers.ContentType.MediaType.ShouldBeEquivalentTo("text/html","HTML is expected be returned");

                var html = await res.Content.ReadAsStringAsync();
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(html);
                htmlDocument.DocumentNode.SelectSingleNode("//h1").InnerText.ShouldBeEquivalentTo("Welcome", "expected 'Welcome' heading");
            }
        }

        [Fact]
        public async void RazorOwinWithLayoutTest()
        {
            using (var server = TestServer.Create<NancyStartup>())
            {
                var req = server.CreateRequest("/razorWithLayout/Colin");
                var res = await req.GetAsync();

                res.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.OK, "expecting an ok response");
                res.Content.Headers.ContentType.MediaType.ShouldBeEquivalentTo("text/html", "HTML is expected be returned");

                var html = await res.Content.ReadAsStringAsync();
                var htmlDocument = new HtmlDocument();
                htmlDocument.LoadHtml(html);
                htmlDocument.DocumentNode.SelectSingleNode("//h1").InnerText.ShouldBeEquivalentTo("Hi Colin", "expected the correct heading text");
            }
        }
    }
}