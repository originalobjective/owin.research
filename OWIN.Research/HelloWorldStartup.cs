﻿using Owin;

namespace OWIN.Research
{
    public class HelloWorldStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Run(context => context.Response.WriteAsync("Hello World"));
        }
    }
}
