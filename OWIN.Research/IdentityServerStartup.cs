﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using Owin;
using Thinktecture.IdentityServer.Core.Configuration;
using Thinktecture.IdentityServer.Core.Models;
using Thinktecture.IdentityServer.Core.Services.InMemory;

namespace OWIN.Research
{
    public class IdentityServerStartup
    {
        private readonly X509Certificate2 cert;

        public IdentityServerStartup(X509Certificate2 cert)
        {
            this.cert = cert;
        }

        public void Configuration(IAppBuilder appBuilder)
        {
            var factory = InMemoryFactory.Create(
                new[] {new InMemoryUser {Username = "colin", Password = "password", Enabled = true, Subject = "12345", Claims = new [] { new Claim("special101","admin"), }}}
                    .ToList(),
                new[]
                {
                    new Client
                    {
                        Enabled = true,
                        ClientId = "chw",
                        ClientName = "ColsTest",
                        Flow = Flows.ClientCredentials,
                        ClientSecrets = new[] {new ClientSecret("secret".Sha512())}.ToList(),
                        ScopeRestrictions = new List<string>(new[] {"profile","special_access"}),
                        Claims = new[] {new Claim("chw:delete","access")}.ToList(),
                        AccessTokenLifetime = 360,
                        AccessTokenType = AccessTokenType.Jwt
                    },
                    new Client
                    {
                        Enabled = true,
                        ClientId = "chw_resource",
                        ClientName = "ColsTest",
                        Flow = Flows.ResourceOwner,
                        ClientSecrets = new[] {new ClientSecret("secret".Sha512())}.ToList(),
                        ScopeRestrictions = new List<string>(new[] {"profile","special_access"}),
                        Claims = new[] {new Claim("chw:delete","access")}.ToList(),
                        AccessTokenLifetime = 360,
                        AccessTokenType = AccessTokenType.Jwt
                    },
                    new Client
                    {
                        Enabled = true,
                        ClientId = "chw_implicit",
                        RequireConsent = false,
                        ClientName = "ColsTest",
                        Flow = Flows.Implicit,
                        ClientSecrets = new[] {new ClientSecret("secret".Sha512())}.ToList(),
                        ScopeRestrictions = new List<string>(new[] {"openid","profile","special_access"}),
                        AccessTokenLifetime = 360,
                        AccessTokenType = AccessTokenType.Jwt,
                        RedirectUris = new [] { "http://localhost/Foo"}.ToList()
                    },
                    new Client
                    {
                        Enabled = true,
                        ClientId = "chw_code",
                        RequireConsent = false,
                        ClientName = "ColsTest",
                        Flow = Flows.AuthorizationCode,
                        ClientSecrets = new[] {new ClientSecret("secret".Sha512())}.ToList(),
                        ScopeRestrictions = new List<string>(new[] {"openid","profile","special_access"}),
                        AccessTokenLifetime = 360,
                        AccessTokenType = AccessTokenType.Jwt,
                        RedirectUris = new [] { "http://localhost/Foo"}.ToList()
                    },
                     new Client
                    {
                        Enabled = true,
                        ClientId = "chw_hybrid",
                        RequireConsent = false,
                        ClientName = "ColsTest",
                        Flow = Flows.Hybrid,
                        ClientSecrets = new[] {new ClientSecret("secret".Sha512())}.ToList(),
                        ScopeRestrictions = new List<string>(new[] {"openid","profile","special_access"}),
                        AccessTokenLifetime = 360,
                        AccessTokenType = AccessTokenType.Jwt,
                        RedirectUris = new [] { "http://localhost/Foo"}.ToList()
                    }
                },
                new[]
                {
                    new Scope {Name = "profile"},
                    new Scope {Name = "openid", Type = ScopeType.Identity},
                    new Scope {Name = "special_access", Claims = new[] {new ScopeClaim("special101", true) }.ToList()}
                });

            var options = new IdentityServerOptions
            {
                IssuerUri = "https://idsrv3.com",
                SiteName = "Thinktecture IdentityServer v3 - preview 1 (SelfHost)",
                Factory = factory,
                RequireSsl = false,
                SigningCertificate = cert,
                PublicOrigin = "http://idp"
            };

            appBuilder.UseIdentityServer(options);
        }
    }
}
