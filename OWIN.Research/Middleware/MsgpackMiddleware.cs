﻿using System.IO;
using System.Threading.Tasks;
using Microsoft.Owin;
using MsgPack;
using MsgPack.Serialization;
using OWIN.Research.Models;

namespace OWIN.Research.Middleware
{
    public class MsgpackMiddleware : OwinMiddleware
    {
        public MsgpackMiddleware(OwinMiddleware next) : base(next)
        {
        }

        public override Task Invoke(IOwinContext context)
        {
            return Task.Factory.StartNew(() =>
            {
                var unpack = Unpacker.Create(context.Request.Body);
                unpack.UnpackSubtreeData();
                var ms = new MemoryStream();
              
                MessagePackSerializer.Get<Hello>().Pack(ms, new Hello { Name = "Colin" });
                ms.Seek(0, 0);
                return context.Response.WriteAsync(ms.ToArray());
            });
        }
    }
}