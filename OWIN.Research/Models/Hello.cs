﻿namespace OWIN.Research.Models
{
    public class Hello
    {
        /// <summary>
        /// Who we are greeting.
        /// </summary>
        public string Name { get; set; }

        public Planet Planet { get; set; }
    }
}
