﻿namespace OWIN.Research.Models
{
    public enum Planet
    {
        Mercury,
        Venus,
        Earth
    }
}