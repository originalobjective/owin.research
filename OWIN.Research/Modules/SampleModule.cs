﻿using Nancy;

namespace OWIN.Research.Modules
{
    public class SampleModule : NancyModule
    {
        public SampleModule()
        {
            Get["/hello"] = o => "Hello";
            Get["/razor"] = o => View["Sample"];
            Get["/razorWithLayout/{name}"] = o => View["SampleWithLayout", new {o.name}];
        }
    }
}
