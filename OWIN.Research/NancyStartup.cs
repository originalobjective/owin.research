﻿using Owin;

namespace OWIN.Research
{
    public class NancyStartup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseNancy();
        }
    }
}
