﻿using System.Collections.Generic;
using Nancy.ViewEngines.Razor;

namespace OWIN.Research
{
    /// <summary>
    /// This Razor config is discovered dynamically.
    /// </summary>
    public class RazorConfig : IRazorConfiguration
    {
        public IEnumerable<string> GetAssemblyNames()
        {
            yield return "OWIN.Research";
        }

        public IEnumerable<string> GetDefaultNamespaces()
        {
            yield return "OWIN.Research";
            yield return "Nancy.ViewEngines.Razor";
        }

        public bool AutoIncludeModelNamespace
        {
            get { return true; }
        }
    }
}